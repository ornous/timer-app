# timer

A timer app to experiment with timing things on the frontend and the backend

Completion Criteria

# Install
```
git clone
yarn install
```

# Run
## Dev Mode
```
  yarn start
  yarn webpack
  yarn serve build

## Production
TBD