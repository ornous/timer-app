type state = {
  seconds: int,
  isTicking: bool
};

let padNumber = numString =>
  if (numString |> int_of_string < 10) {
    "0" ++ numString;
  } else {
    numString;
  };

let formatTime = seconds => {
  let mins = seconds / 60;
  let minsString = mins |> string_of_int |> padNumber;
  let seconds = seconds mod 60;
  let secondsString = seconds |> string_of_int |> padNumber;
  minsString ++ ":" ++ secondsString;
};

type action =
| Start
| Stop
| Reset
| Tick;

module Styles = {
  open Css;

  let actionButton = disabled =>
    style([
      background(disabled ? darkgray : white),
      color(black),
      border(px(1), solid, black),
      borderRadius(px(3)),
      fontSize(em(0.6))
    ])

  let controls = style([
    display(flexBox),
    flexDirection(row),
    justifyContent(spaceEvenly),
  ])

  let card = style([
    border(px(1), solid, black),
    borderRadius(px(3)),
    width(auto),
    unsafe("width", "max-content"),
    fontSize(rem(1.5)),
    textAlign(center),
    padding2(~v=em(1.0), ~h=em(1.0)),
    margin2(~v=px(0), ~h=auto),
    boxShadow(~y=px(3), ~blur=px(5), rgba(0, 0, 0, 0.3)),
  ])

  let duration = style([
    color(hex("#444444")),
    fontSize(em(2.0)),
    margin(em(0.5)),
  ])
}

module Button = {
  [@react.component]
  let make = (~label, ~onClick) => {
    <button
      className=Styles.actionButton(false)
      onClick> {label |> ReasonReact.string} </button>;
  };
};

[@react.component]
let make = () => {
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
      switch (action) {
        | Start => {...state, isTicking: true}
        | Stop => {...state, isTicking: false}
        | Reset => {...state, seconds: 30}
        | Tick => state.isTicking && state.seconds > 0
            ? {...state, seconds: state.seconds - 1} : state
        },
        {isTicking: false, seconds: 30},
    );

  React.useEffect0(() => {
    let timerId = Js.Global.setInterval(() => dispatch(Tick), 1000);
    Some(() => Js.Global.clearInterval(timerId));
  });

  <div className=Styles.card>
    <p className=Styles.duration>
      {state.seconds |> formatTime |> ReasonReact.string}
    </p>
    <div className=Styles.controls>
    {state.isTicking
       ? <Button label="STOP" onClick={_event => dispatch(Stop)} />
       : <>
           <Button label="START" onClick={_event => dispatch(Start)} />
           <Button label="RESET" onClick={_event => dispatch(Reset)} />
         </>}
    </div>
  </div>;

};
